<?php

namespace Drupal\find_and_replace\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "find" plugin.
 *
 * @CKEditorPlugin(
 *   id = "find",
 *   label = @Translation("Find"),
 *   module = "find"
 * )
 */
class Find extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return '/libraries/find/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return array(
      'core/drupal.ajax',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array(
      'findDialog' => t('Find'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {

    return array(
      'Find' => array(
        'label' => t('Find'),
        'image' => '/libraries/find/icons/find.png',
      ),
      'Replace' => array(
        'label' => t('Replace'),
        'image' => '/libraries/find/icons/replace.png',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $form_state->loadInclude('editor', 'admin.inc');
    return $form;
  }

}
